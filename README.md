# Technical Test - Symfony 


## Run Locally

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/) (v2.10+)

2. Clone the project

 ``` bash
  git clone https://gitlab.com/technical-test-hellosafe/symfony.git && cd symfony
 ```

3. Build fresh images
 ``` bash
docker compose build --no-cache
 ```
4. Start the project

``` bash
docker compose up --pull --wait
```

5. Ensure that all composer dependencies are installed
``` bash
docker compose exec php composer install
```
6. Open https://localhost in your web browser and accept the auto-generated TLS certificate to see the Symfony project home page

7. The endpoint to get a random cat fact is `/v1/api/test-content`

8. Run `docker compose down --remove-orphans` to stop the Docker containers.


## Acknowledgements

 - [Symfony Docker](https://github.com/dunglas/symfony-docker)  by Kévin Dunglas
