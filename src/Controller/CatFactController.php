<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CatFactController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
    
    #[Route('v1/api/test-content', name: 'app_cat_fact')]
    public function index(): JsonResponse
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://catfact.ninja/fact'
            );

            $statusCode = $response->getStatusCode();
            $content = $response->toArray();

            if (!is_array($content)) {
                throw new \Exception('Invalid response format');
            }

            if ($statusCode !== 200) {
                throw new \Exception('Invalid response status code');
            }

            return $this->json($content);
        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()], 500);
        }
    }
}
